import React from 'react';
import {ImageBackground, StyleSheet, Text, View, Image} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Produk from './produk';

const ModalProduk = () => {
  return (
    <View style={{width: 300, height: 350, borderRadius: 20}}>
      <View
        style={{
          justifyContent: 'space-evenly',
          marginBottom: 20,
          flexDirection: 'row',
          marginLeft: 75,
        }}>
        <Text style={{fontSize: 18, fontWeight: '700'}}>Produk Terlaris</Text>
        <FontAwesome5
          name="times"
          size={14}
          style={{color: '#AAA', marginLeft: 85}}
        />
      </View>
      <Produk />
    </View>
  );
};

export default ModalProduk;

const styles = StyleSheet.create({});
