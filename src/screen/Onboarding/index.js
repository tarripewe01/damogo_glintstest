import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Colors from '../../utils/Colors';
import Swiper from 'react-native-swiper';

const OnboardingScreen = ({navigation}) => {
  const [selectedLanguage, setSelectedLanguage] = useState();

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.Base} barStyle="dark-content" />
      <Picker
        selectedValue={selectedLanguage}
        onValueChange={(itemValue, itemIndex) => setSelectedLanguage(itemValue)}
        style={styles.picker}>
        <Picker.Item label="Bahasa" value="bahasa" />
        <Picker.Item label="English" value="english" />
      </Picker>

      <Swiper style={styles.wrapper}>
        <View style={styles.slide1}>
          <Image
            style={styles.image}
            source={require('../../assets/images/onboarding1.png')}
          />
          <Text style={styles.text}>Tingkatkan Penjualan Kamu</Text>
          <Text style={styles.text1}>
            Yuk jual kembali makanan kamu yang belum laku dan hampir terbuang.
            Sekaligus selamatkan lingkungan sekitar kamu dari food waste!
          </Text>
          <View style={styles.text2}>
            <Text
              style={styles.text3}
              onPress={() => navigation.navigate('Login')}>
              Lewati
            </Text>
          </View>
        </View>
        <View style={styles.slide1}>
          <Image
            style={styles.image}
            source={require('../../assets/images/onboarding2.png')}
          />
          <Text style={styles.text}>Dapetin Customer</Text>
          <Text style={styles.text1}>
            Tingkatkan exposure gerai kamu dan branding perusahaan dengan
            aplikasi kami
          </Text>
        </View>
        <View style={styles.slide1}>
          <Image
            style={styles.image}
            source={require('../../assets/images/onboarding3.png')}
          />
          <Text style={styles.text}>Platform Marketing Gratis!</Text>
          <Text style={styles.text1}>
            Perkuat branding gerai kamu dengan menunjukkan ke masyarakat luas
            bahwa kamu peduli terhadap problema food waste, sekaligus
            bertanggung jawab sebagai owner dari usaha yang kamu manage.
          </Text>
        </View>
        <View style={styles.slide1}>
          <Image
            style={styles.image}
            source={require('../../assets/images/onboarding4.png')}
          />
          <Text style={styles.text}>Tanpa Risk</Text>
          <Text style={styles.text1}>
            Semua gratis. Gratis biaya pendaftaran, ga ada biaya bulanan, ga ada
            syarat minimum, ga ada biaya yang tersembunyi. Kurang apa coba?
          </Text>
          <View style={styles.text2}>
            <Text
              style={styles.text3}
              onPress={() => navigation.navigate('Login')}>
              Selesai
            </Text>
          </View>
        </View>
      </Swiper>
    </View>
  );
};

export default OnboardingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Base,
    paddingHorizontal: 20,
  },
  picker: {
    width: 158,
    height: 37,
    color: '#aaa',
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    margin: 10,
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //   backgroundColor: '#97CAE5'
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#00A3E0',
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 5,
  },
  text1: {
    color: '#A0A0A0',
    fontSize: 16,
    // fontWeight: 'bold',
    textAlign: 'center',
  },
  text2: {
    position: 'absolute',
    bottom: 15,
    right: 20,
  },
  text3: {
    color: '#00A3E0',
    fontSize: 18,
  },
  image: {
    width: 325,
    height: 325,
    marginBottom: 15,
  },
});
