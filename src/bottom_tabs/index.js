import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';
import HomeScreen from '../screen/Home';
import ProdukScreen from '../screen/Produk';
import SupplyScreen from '../screen/Supply';
import StatistikScreen from '../screen/Statistik';
import AkunScreen from '../screen/Akun';

const Tab = createBottomTabNavigator();

const MainTab = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={({route}) => ({
        tabBarIcon: ({color}) => {
          let iconName;

          if (route.name === 'Produk') {
            iconName = 'content-copy';
          } else if (route.name === 'Home') {
            iconName = 'home';
          } else if (route.name === 'Akun') {
            iconName = 'account-circle';
          } else if (route.name === 'Statistik') {
            iconName = 'stacked-bar-chart';
          } else if (route.name === 'Supply') {
            iconName = 'shopping-basket';
          }

          return <Icon name={iconName} size={25} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: '#00A5f4',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Produk" component={ProdukScreen} />
      <Tab.Screen name="Supply" component={SupplyScreen} />
      <Tab.Screen name="Statistik" component={StatistikScreen} />
      <Tab.Screen name="Akun" component={AkunScreen} />
    </Tab.Navigator>
  );
};

export default MainTab;

const styles = StyleSheet.create({});
