import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {BottomSheet} from 'react-native-elements/dist/bottomSheet/BottomSheet';
import Colors from '../../../../utils/Colors';
import CaraPakai from '../CaraPakai';
import CustomerSupport from '../Customer';
import DaftarPengguna from '../DaftarPengguna';
import KebijakanPublik from '../Kebijakan';
import KebijakanPengguna from '../KebijakanPengguna';
import Pengaturan from '../Pengaturan';

const Content = () => {
  return (
    <View style={styles.container}>
      <Pengaturan />
      <CustomerSupport />
      <CaraPakai />
      <KebijakanPublik />
      <KebijakanPengguna />
      <DaftarPengguna />
      <TouchableOpacity>
        <View style={styles.button}>
          <Text style={styles.textButton}>Keluar</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
``;
export default Content;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.Base,
    marginTop: 10,
    width: '100%',
    paddingHorizontal: 10,
    height: 400,
  },
  button: {
    borderColor: Colors.Danger,
    borderWidth: 1,
    height: 48,
    marginTop: 60,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButton: {
    color: Colors.Danger,
    fontSize: 15,
    fontWeight: '700',
    lineHeight: 17.58,
    letterSpacing: 0.4,
  },
});
