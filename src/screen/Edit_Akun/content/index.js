import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Colors from '../../../utils/Colors/index';
import AlamatToko from '../components/AlamatToko';
import DeskripsiToko from '../components/Deskripsi Toko';
import Jabatan from '../components/Jabatan';
import NamaToko from '../components/NamaToko';

const ContentProfileToko = () => {
  return (
    <View style={styles.container}>
      <NamaToko />
      <AlamatToko />
      <DeskripsiToko/>
      <Jabatan />
     
    </View>
  );
};
``;
export default ContentProfileToko;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.Base,
    marginTop: 10,
    width: '100%',
    paddingHorizontal: 10,
    height: 200,
  },
  button: {
    borderColor: Colors.Danger,
    borderWidth: 1,
    height: 48,
    marginTop: 60,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButton: {
    color: Colors.Danger,
    fontSize: 15,
    fontWeight: '700',
    lineHeight: 17.58,
    letterSpacing: 0.4,
  },
});
