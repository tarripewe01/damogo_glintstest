import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../utils/Colors';

const HeaderToko = () => {
  return (
    <ScrollView>
        <View styles={styles.containerBg}>
          <View>
            <ImageBackground
              source={require('../../assets/images/salad.png')}
              style={styles.foto}>
              <ImageBackground
                source={require('../../assets/images//blur.png')}
                style={styles.foto}>
                <View style={{marginTop: 50}}>
                  <View>
                    <ImageBackground
                      source={require('../../assets/images/saladblur.png')}
                      style={styles.profile}>
                      <TouchableOpacity onPress={() => {}}>
                        <FontAwesome5
                          name="camera"
                          size={16}
                          style={{color: '#fff'}}
                        />
                      </TouchableOpacity>
                    </ImageBackground>
                  </View>
                </View>
              </ImageBackground>
            </ImageBackground>
            <View style={styles.containerGantiSampul}>
              <TouchableOpacity>
                <ImageBackground
                  source={require('../../assets/images/gantisampul.png')}
                  style={{
                    height: 24,
                    width: 115,
                  }}>
                  <Text style={styles.textGantiSampul}>Ganti foto sampul </Text>
                </ImageBackground>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        
    </ScrollView>
  );
};

export default HeaderToko;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: Colors.Danger,
  },
  foto: {
    // alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    resizeMode: 'contain',
    width: '100%',
    height: 226,
  },
  foto2: {
    alignItems: 'center',
    justifyContent: 'center',
    resizeMode: 'contain',
    width: '100%',
    height: 226,
  },
  profile: {
    width: 69,
    height: 69,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 65,
  },
  containerGantiSampul: {
    alignItems: 'flex-end',
    paddingRight: 10,
    marginTop: -40,
  },
  textGantiSampul: {
    fontSize: 12,
    fontWeight: '400',
    lineHeight: 24,
    letterSpacing: 0.4,
    color: Colors.Base,
    alignSelf: 'center',
  },
});
