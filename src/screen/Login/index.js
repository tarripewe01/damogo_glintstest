import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Colors from '../../utils/Colors';

const LoginScreen = ({navigation}) => {
  const [selectedLanguage, setSelectedLanguage] = useState();
  return (
    <View style={styles.container}>
      <View style={styles.containerPicker}>
        <Picker
          selectedValue={selectedLanguage}
          onValueChange={(itemValue, itemIndex) =>
            setSelectedLanguage(itemValue)
          }
          style={styles.picker}>
          <Picker.Item label="Bahasa" value="bahasa" />
          <Picker.Item label="English" value="english" />
        </Picker>
      </View>
      <Image
        source={require('../../assets/images/logo.png')}
        style={styles.logo}
      />
      <Text style={styles.textLogin}>Login</Text>
      <TextInput style={styles.form} placeholder="Email" />
      <TextInput
        style={styles.form}
        placeholder="Kata Sandi"
        secureTextEntry={true}
      />
      <TouchableOpacity
        style={styles.containerBtn}
        onPress={() => navigation.navigate('Home')}>
        <Text style={styles.textBtn}>Masuk</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.containerBtn2}>
        <Text style={styles.textBtn2}>Daftar sebagai mitra</Text>
      </TouchableOpacity>
      <Text style={styles.term}>
        Dengan masuk, kamu menyetujui Kebijakan Penggunaan dan Kebijakan Publik
        kami
      </Text>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Base,
    paddingHorizontal: 15,
  },
  containerPicker: {
    borderWidth: 1,
    borderColor: '#aaa',
    width: 158,
    height: 30,
    marginTop: 20,
  },
  picker: {
    width: 158,
    height: 37,
    color: '#aaa',
    borderWidth: 1,
    borderColor: '#aaa',
    marginTop: -13,
  },
  logo: {
    width: 144,
    height: 127,
    alignSelf: 'center',
    marginTop: 30,
  },
  textLogin: {
    fontSize: 24,
    fontWeight: '700',
    lineHeight: 32.81,
    letterSpacing: 0.8,
    marginTop: 10,
    marginBottom: 5
  },
  form: {
    borderBottomWidth: 1,
    borderBottomColor: '#D8D8D8',
    paddingBottom: 3,
    marginTop: 15,
  },
  containerBtn: {
    backgroundColor: Colors.Primary,
    height: 48,
    borderRadius: 3,
    marginTop: 25,
  },
  textBtn: {
    alignSelf: 'center',
    paddingVertical: 10,
    color: Colors.Base,
    fontSize: 15,
    fontWeight: '700',
    lineHeight: 24,
    letterSpacing: 0.4,
  },
  containerBtn2: {
    borderWidth: 1,
    borderColor: Colors.Primary,
    height: 48,
    marginTop: 10,
    borderRadius: 3,
  },
  textBtn2: {
    alignSelf: 'center',
    paddingVertical: 10,
    color: Colors.Primary,
    fontSize: 15,
    fontWeight: '700',
    lineHeight: 24,
    letterSpacing: 0.4,
  },
  term: {
    fontSize: 13,
    fontWeight: '400',
    lineHeight: 17.58,
    letterSpacing: 0.4,
    color: Colors.Text,
    textAlign: 'center',
    marginTop: 20,
  },
});
