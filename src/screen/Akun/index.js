import React from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import Colors from '../../utils/Colors';
import Content from './component/content';
import HeaderProfile from './component/HeaderProfile';
import RatingToko from './component/RatingToko';

const AkunScreen = ({navigation}) => {
  return (
    <ScrollView>
      <View styles={styles.containerBg}>
        <View>
          <ImageBackground
            source={require('../../assets/images/salad.png')}
            style={styles.foto}>
            <ImageBackground
              source={require('../../assets/images/aksen.png')}
              style={styles.foto2}>
              <View style={{marginTop: 50}}>
                <View
                  style={{
                    marginLeft: 130,
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Image
                    source={require('../../assets/images/profile.png')}
                    style={styles.profile}
                  />
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Edit Profil Toko')}>
                    <FontAwesome5
                      name="edit"
                      size={16}
                      style={{color: '#fff', marginBottom: 40}}
                    />
                  </TouchableOpacity>
                </View>
                <View>
                  <Text style={styles.profileName}>Salad Moi</Text>
                </View>

                <View style={styles.containerIcon}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    <FontAwesome5
                      name="user-alt"
                      color={Colors.Base}
                      size={16}
                    />
                    <Text style={styles.icon}>100 Pengikut</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingLeft: 10,
                    }}>
                    <FontAwesome5
                      name="instagram"
                      color={Colors.Base}
                      size={16}
                    />
                    <Text style={styles.icon}>@saladmoi</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingLeft: 10,
                    }}>
                    <FontAwesome5
                      name="phone-alt"
                      color={Colors.Base}
                      size={16}
                    />
                    <Text style={styles.icon}>089745624982</Text>
                  </View>
                </View>
                <View style={{alignItems: 'center'}}>
                  <Text style={styles.address}>
                    Gg. Mbakalan No 14, Jl. Kaliurang No.km 7.6
                  </Text>
                </View>
              </View>
            </ImageBackground>
          </ImageBackground>
        </View>
      </View>
      <View style={styles.container}>
        <RatingToko />

        <Content />
      </View>
    </ScrollView>
  );
};

export default AkunScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: Colors.Danger,
  },
  container: {
    flex: 1,
    color: Colors.Danger,
  },
  foto: {
    // alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    resizeMode: 'contain',
    width: '100%',
    height: 226,
  },
  foto2: {
    alignItems: 'center',
    justifyContent: 'center',
    resizeMode: 'contain',
    width: '100%',
    height: 226,
  },
  profile: {
    width: 69,
    height: 69,
  },
  profileName: {
    color: Colors.Base,
    fontSize: 18,
    fontWeight: '700',
    lineHeight: 24,
    letterSpacing: 0.4,
    textAlign: 'center',
    paddingTop: 5,
  },
  containerIcon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  icon: {
    color: Colors.Base,
    fontSize: 13,
    fontWeight: '500',
    lineHeight: 24,
    letterSpacing: 0.4,
    paddingLeft: 5,
  },
  address: {
    color: Colors.Base,
    fontSize: 12,
    fontWeight: '500',
    lineHeight: 24,
    letterSpacing: 0.4,
  },
});
