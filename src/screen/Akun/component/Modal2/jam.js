import React from 'react';
import {ImageBackground, StyleSheet, Text, View, Image} from 'react-native';

const Jam = () => {
  return (
    <View>
      <View style={{flexDirection: 'row'}}>
        <ImageBackground
          source={require('../../../../assets/images/jamOn.png')}
          style={{height: 30, width: 30}}>
          <Text
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              fontSize: 15,
              fontWeight: '500',
              color: '#fff',
              paddingVertical: 5,
            }}>
            1
          </Text>
        </ImageBackground>

        <View
          style={{marginHorizontal: 10, flexDirection: 'row', marginTop: 5}}>
          <Text style={{fontSize: 15, fontWeight: '500', color: '#A0A0A0'}}>
            12.00 - 16.00
          </Text>
          <View
            style={{
              width: 75,
              height: 24,
              backgroundColor: '#F4F4F4',
              borderRadius: 3,
              marginLeft: 95,
            }}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                color: '#A0A0A0',
                alignSelf: 'center',
                paddingVertical: 3,
              }}>
              24 Terjual
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          borderColor: '#E7E7E7',
          borderBottomWidth: 1,
          marginTop: 15,
        }}></View>

      <View style={{flexDirection: 'row', marginTop: 10}}>
        <ImageBackground
          source={require('../../../../assets/images/jamOff.png')}
          style={{height: 30, width: 30}}>
          <Text
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              fontSize: 15,
              fontWeight: '500',
              color: '#fff',
              paddingVertical: 5,
            }}>
            2
          </Text>
        </ImageBackground>

        <View
          style={{marginHorizontal: 10, flexDirection: 'row', marginTop: 5}}>
          <Text style={{fontSize: 15, fontWeight: '500', color: '#A0A0A0'}}>
            16.00 - 19.00
          </Text>
          <View
            style={{
              width: 75,
              height: 24,
              backgroundColor: '#F4F4F4',
              borderRadius: 3,
              marginLeft: 95,
            }}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                color: '#A0A0A0',
                alignSelf: 'center',
                paddingVertical: 3,
              }}>
              10 Terjual
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          borderColor: '#E7E7E7',
          borderBottomWidth: 1,
          marginTop: 15,
        }}></View>

      <View style={{flexDirection: 'row', marginTop: 10}}>
        <ImageBackground
          source={require('../../../../assets/images/jamOff.png')}
          style={{height: 30, width: 30}}>
          <Text
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              fontSize: 15,
              fontWeight: '500',
              color: '#fff',
              paddingVertical: 5,
            }}>
            3
          </Text>
        </ImageBackground>

        <View
          style={{marginHorizontal: 10, flexDirection: 'row', marginTop: 5}}>
          <Text style={{fontSize: 15, fontWeight: '500', color: '#A0A0A0'}}>
            19.00 - 21.00
          </Text>
          <View
            style={{
              width: 75,
              height: 24,
              backgroundColor: '#F4F4F4',
              borderRadius: 3,
              marginLeft: 95,
            }}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                color: '#A0A0A0',
                alignSelf: 'center',
                paddingVertical: 3,
              }}>
              5 Terjual
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Jam;

const styles = StyleSheet.create({});
