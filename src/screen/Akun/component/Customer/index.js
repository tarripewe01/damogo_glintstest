import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Colors from '../../../../utils/Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const CustomerSupport = () => {
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: 10,
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Image source={require('../../../../assets/images/customer.png')} />

          <Text
            style={{
              paddingLeft: 10,
              fontSize: 15,
              fontWeight: '500',
              lineHeight: 17.58,
              letterSpacing: 0.4,
            }}>
            Customer support
          </Text>
        </View>
        <TouchableOpacity>
          <MaterialCommunityIcons
            name="chevron-right"
            size={20}
            style={{color: '#AAAAAA'}}
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          borderBottomColor: '#F4F4F4',
          borderBottomWidth: 1,
          marginTop: 15,
        }}></View>
    </View>
  );
};

export default CustomerSupport;

const styles = StyleSheet.create({});
