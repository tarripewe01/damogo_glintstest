import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Colors from '../../../../utils/Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const AlamatToko = () => {
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: 10,
        }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text
            style={{
              paddingLeft: 10,
              fontSize: 14,
              fontWeight: '400',
              lineHeight: 17.58,
              letterSpacing: 0.4,
            }}>
            Alamat Toko
          </Text>
        </View>
        <TouchableOpacity style={{flexDirection: 'row'}}>
          <Text
            style={{
              paddingLeft: 10,
              fontSize: 13,
              fontWeight: '400',
              lineHeight: 17.58,
              letterSpacing: 0.4,
            }}>
            Gg. Mbakalan No 14 ...
          </Text>
          <MaterialCommunityIcons
            name="chevron-right"
            size={20}
            style={{color: '#AAAAAA'}}
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          borderBottomColor: '#F4F4F4',
          borderBottomWidth: 1,
          marginTop: 15,
        }}></View>
    </View>
  );
};

export default AlamatToko;

const styles = StyleSheet.create({});
