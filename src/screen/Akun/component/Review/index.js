import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

const Review = () => {
  return (
    <View style={{paddingHorizontal: 10, marginTop: -10}}>
      <View style={{flexDirection: 'row'}}>
        <Image
          source={require('../../../../assets/images/review.png')}
          style={{borderRadius: 30}}
        />
        <View style={{marginLeft: 10}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{fontSize: 15, fontWeight: '400', lineHeight: 24}}>
              Saraswati Putri
            </Text>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '400',
                lineHeight: 24,
                fontStyle: 'italic',
                alignSelf: 'flex-end',
                marginLeft: 100,
              }}>
              1 hari yang lalu
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 3}}>
            <Image
              source={require('../../../../assets/images/starss.png')}
              style={{height: 13}}
            />

            <Text
              style={{
                fontSize: 12,
                fontWeight: '400',
                lineHeight: 14.06,
                marginLeft: 10,
                color: '#AAA',
              }}>
              5.0
            </Text>
          </View>
        </View>
      </View>
      <Text
        style={{
          fontSize: 15,
          fontWeight: '400',
          lineHeight: 17.58,
          color: '#AAA',
          marginTop: 5,
        }}>
        Enak banget humbergernya, gila bikin nagih serius, makasih banyak ya ...
      </Text>
      <View
        style={{
          borderColor: '#E7E7E7',
          borderBottomWidth: 1,
          marginTop: 10,
          marginBottom: 20
        }}></View>
    </View>
  );
};

export default Review;

const styles = StyleSheet.create({});
