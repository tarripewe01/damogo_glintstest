import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const JadwalBuka = () => {
  return (
    <View>
      <View style={{flexDirection: 'column'}}>
        <View style={{backgroundColor: '#fff', height: 130, marginTop: 10}}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                width: 80,
                height: 40,
                marginLeft: 10,
                marginRight: -5,
                alignItems: 'center',
                marginVertical: 20,
                backgroundColor: '#EDEDED',
                borderRadius: 5,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '400',
                  lineHeight: 24,
                  letterSpacing: 0.4,
                  padding: 8,
                  color: '#9A9A9A',
                }}>
                Minggu
              </Text>
            </View>
            <View
              style={{
                width: 80,
                height: 40,
                marginLeft: 10,
                alignItems: 'center',
                marginVertical: 20,
                backgroundColor: '#00A3E0',
                borderRadius: 5,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '400',
                  lineHeight: 24,
                  padding: 8,
                  color: '#fff',
                }}>
                Senin
              </Text>
            </View>
            <View
              style={{
                width: 80,
                height: 40,
                marginLeft: 10,
                alignItems: 'center',
                marginVertical: 20,
                backgroundColor: '#00A3E0',
                borderRadius: 5,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '400',
                  lineHeight: 24,
                  padding: 8,
                  color: '#fff',
                }}>
                Selasa
              </Text>
            </View>
            <View
              style={{
                width: 80,
                height: 40,
                marginLeft: 10,
                alignItems: 'center',
                marginVertical: 20,
                backgroundColor: '#00A3E0',
                borderRadius: 5,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '400',
                  lineHeight: 24,
                  padding: 8,
                  color: '#fff',
                }}>
                Rabu
              </Text>
            </View>
          </View>
          <View style={{flexDirection:'row', alignSelf: 'center', marginTop: -30}}>
            <View
              style={{
                width: 80,
                height: 40,
                marginLeft: 10,
                alignItems: 'center',
                marginVertical: 20,
                backgroundColor: '#00A3E0',
                borderRadius: 5,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '400',
                  lineHeight: 24,
                  padding: 8,
                  color: '#fff',
                }}>
                Kamis
              </Text>
            </View>
            <View
              style={{
                width: 80,
                height: 40,
                marginLeft: 10,
                marginRight: -5,
                alignItems: 'center',
                marginVertical: 20,
                backgroundColor: '#EDEDED',
                borderRadius: 5,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '400',
                  lineHeight: 24,
                  letterSpacing: 0.4,
                  padding: 8,
                  color: '#9A9A9A',
                }}>
                Jumat
              </Text>
            </View>
            <View
              style={{
                width: 80,
                height: 40,
                marginLeft: 10,
                marginRight: -5,
                alignItems: 'center',
                marginVertical: 20,
                backgroundColor: '#EDEDED',
                borderRadius: 5,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: '400',
                  letterSpacing: 0.4,
                  padding: 8,
                  color: '#9A9A9A',
                }}>
                Sabtu
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default JadwalBuka;

const styles = StyleSheet.create({});
