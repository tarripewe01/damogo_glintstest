import React, {useRef, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ListItem,
  ScrollView,
} from 'react-native';
import {Button, Overlay} from 'react-native-elements';
import RBSheet from 'react-native-raw-bottom-sheet';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../../../utils/Colors';
import ModalProduk from '../Modal';
import Modal2 from '../Modal2/index';
import Review from '../Review';

const RatingToko = () => {
  const [visible, setVisible] = useState(false);
  const [visible2, setVisible2] = useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };
  const toggleOverlay2 = () => {
    setVisible2(!visible2);
  };

  const toggleBottomSheet = () => {
    setIsVisible(!isVisible);
  };

  const refRBSheet = useRef();

  return (
    <ScrollView>
      <View style={{backgroundColor: Colors.Base, width: '100%', height: 110}}>
        <View style={styles.containerAction}>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.rating}>Rating Toko</Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginLeft: 5,
              }}>
              <Image source={require('../../../../assets/images/star.png')} />
              <Text style={styles.rate}>5.0</Text>
            </View>
          </View>
          <TouchableOpacity>
            <RBSheet
            height={500}
              ref={refRBSheet}
              closeOnDragDown={true}
              closeOnPressMask={false}>
              
              <View>
                <View
                  style={{
                    justifyContent: 'space-evenly',
                    marginBottom: 20,
                    flexDirection: 'row',
                    marginLeft: 120,
                  }}>
                  <Text style={{fontSize: 18, fontWeight: '700'}}>Ulasan</Text>
                  <FontAwesome5
                    name="times"
                    size={14}
                    style={{color: '#AAA', marginLeft: 120}}
                  />
                </View>
                <Review />
                <Review />
                <Review />
                <Review />
              </View>
            </RBSheet>

            <Text
              style={styles.ulasan}
              onPress={() => refRBSheet.current.open()}>
              Lihat ulasan
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row', marginHorizontal: 5}}>
          <View style={styles.containerthumb}>
            <TouchableOpacity title="Open Overlay" onPress={toggleOverlay}>
              <Overlay isVisible={visible} onBackdropPress={toggleOverlay}>
                <ModalProduk />
              </Overlay>
              <MaterialCommunityIcons
                name="thumb-up"
                size={23}
                style={styles.thumbIcon}
              />
              <Text style={styles.textThumb}>3 Produk terlaris</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.containerAlarm}>
            <TouchableOpacity title="Open Overlay" onPress={toggleOverlay2}>
              <Overlay isVisible={visible2} onBackdropPress={toggleOverlay2}>
                <Modal2 />
              </Overlay>
              <MaterialCommunityIcons
                name="alarm"
                size={23}
                style={styles.alarmIcon}
              />
              <Text style={styles.textAlarm}>Jam paling recomended</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default RatingToko;

const styles = StyleSheet.create({
  rating: {
    paddingRight: 10,
    fontSize: 15,
    fontWeight: '700',
    lineHeight: 24,
  },
  rate: {
    paddingLeft: 5,
    color: Colors.Text,
    fontSize: 13,
    fontWeight: '500',
    lineHeight: 24,
    letterSpacing: 0.4,
  },
  ulasan: {
    color: Colors.Warning,
    fontSize: 15,
    fontWeight: '700',
    lineHeight: 24,
    letterSpacing: 0.4,
  },
  containerAction: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    marginTop: 5,
  },
  containerthumb: {
    backgroundColor: Colors.BaseWarning,
    width: 170,
    height: 64,
    alignItems: 'center',
    justifyContent: 'center',
    // marginHorizontal: 15,
    borderRadius: 4,
    marginTop: 5,
  },
  thumbIcon: {
    color: Colors.Warning,
    alignSelf: 'center',
    marginBottom: 5,
  },
  textThumb: {
    color: Colors.Warning,
    fontSize: 15,
    fontWeight: '500',
    lineHeight: 24,
    letterSpacing: 0.4,
    marginBottom: 5,
  },
  containerAlarm: {
    backgroundColor: Colors.BaseSave,
    width: 175,
    height: 64,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 15,
    borderRadius: 4,
    marginTop: 5,
    marginLeft: 5,
  },
  alarmIcon: {
    color: Colors.Save,
    alignSelf: 'center',
    marginBottom: 5,
  },
  textAlarm: {
    color: Colors.Save,
    fontSize: 15,
    fontWeight: '500',
    lineHeight: 24,
    letterSpacing: 0.4,
    marginBottom: 5,
  },
});
