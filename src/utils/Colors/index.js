const Colors = {
    Primary : '#00A3E0',
    Base : '#fff',
    Danger : '#FF4A4A',
    Warning : '#FFBB00',
    BaseWarning : '#FFF2DE',
    Save : '#009673',
    BaseSave : '#DAFFD9',
    Text: '#A0A0A0'
}

export default Colors;