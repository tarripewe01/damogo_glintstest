import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const JamOperasiona = () => {
    return (
        <View>
            <View style={{backgroundColor: '#fff', height: 80, marginTop: 10}}>
        <View style={{flexDirection: 'row'}}>
          <View
            style={{
              borderWidth: 1,
              borderColor: '#AAA',
              width: 100,
              height: 40,
              marginLeft: 10,
              alignItems: 'center',
              marginVertical: 20,
              borderRadius: 3
            }}>
            <Text
              style={{
                fontSize: 15,
                fontWeight: '400',
                lineHeight: 24,
                letterSpacing: 0.4,
                padding: 8,
              }}>
              06.00 Am
            </Text>
          </View>
          <View
            style={{
              borderWidth: 1,
              borderColor: '#AAA',
              width: 100,
              height: 40,
              marginLeft: 10,
              alignItems: 'center',
              marginVertical: 20,
              borderRadius: 3
            }}>
            <Text
              style={{
                fontSize: 15,
                fontWeight: '400',
                lineHeight: 24,
                letterSpacing: 0.4,
                padding: 8,
              }}>
              12.00 Pm
            </Text>
          </View>
        </View>
      </View>
        </View>
    )
}

export default JamOperasiona

const styles = StyleSheet.create({})
