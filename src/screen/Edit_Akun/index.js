import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {colors} from 'react-native-elements';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import HeaderToko from '../../components/HeaderToko';
import Colors from '../../utils/Colors';
import ContentEdit from '../Akun/component/content';
import JadwalBuka from './components/Jadwal Buka';
import JamOperasiona from './components/Jam Operasional';
import ContentProfileToko from './content';

const EditAkun = ({navigation}) => {
  return (
    <ScrollView>
      <HeaderToko />
      <ContentProfileToko />
      <View style={{marginLeft: 10, marginTop: 10}}>
        <Text style={styles.text}>Edit jam operasional</Text>
      </View>

      <JamOperasiona />

      <View style={{marginLeft: 10, marginTop: 10}}>
        <Text style={styles.text}>Edit jadwal hari buka </Text>
      </View>

      <JadwalBuka />

      <View
        style={{
          backgroundColor: '#fff',
          marginTop: 50,
          height: 80,
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        
          <View
            style={{
              backgroundColor: '#00A3E0',
              height: 48,
              width: '90%',
              justifyContent: 'center',
              borderRadius: 3,
              alignItems: 'center',
            }}>
              <TouchableOpacity onPress={() => navigation.navigate('Akun')}>
            <Text style={{fontSize: 15, fontWeight: '700', color: '#fff'}}>
              Simpan
            </Text>
            </TouchableOpacity>
          </View>
      </View>
      {/* <View styles={styles.containerBg}>
          <View>
            <ImageBackground
              source={require('../../assets/images/salad.png')}
              style={styles.foto}>
              <ImageBackground
                source={require('../../assets/images//blur.png')}
                style={styles.foto}>
                <View style={{marginTop: 50}}>
                  <View>
                    <ImageBackground
                      source={require('../../assets/images/saladblur.png')}
                      style={styles.profile}>
                      <TouchableOpacity onPress={() => {}}>
                        <FontAwesome5
                          name="camera"
                          size={16}
                          style={{color: '#fff'}}
                        />
                      </TouchableOpacity>
                    </ImageBackground>
                  </View>
                </View>
              </ImageBackground>
            </ImageBackground>
            <View style={styles.containerGantiSampul}>
              <TouchableOpacity>
                <ImageBackground
                  source={require('../../assets/images/gantisampul.png')}
                  style={{
                    height: 24,
                    width: 115,
                  }}>
                  <Text style={styles.textGantiSampul}>Ganti foto sampul </Text>
                </ImageBackground>
              </TouchableOpacity>
            </View>
          </View>
        </View> */}
    </ScrollView>
  );
};

export default EditAkun;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: Colors.Danger,
  },
  foto: {
    // alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    resizeMode: 'contain',
    width: '100%',
    height: 226,
  },
  foto2: {
    alignItems: 'center',
    justifyContent: 'center',
    resizeMode: 'contain',
    width: '100%',
    height: 226,
  },
  profile: {
    width: 69,
    height: 69,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 65,
  },
  containerGantiSampul: {
    alignItems: 'flex-end',
    paddingRight: 10,
    marginTop: -40,
  },
  textGantiSampul: {
    fontSize: 12,
    fontWeight: '400',
    lineHeight: 24,
    letterSpacing: 0.4,
    color: Colors.Base,
    alignSelf: 'center',
  },
  text: {
    fontSize: 18,
    fontWeight: '700',
    lineHeight: 21.09,
    letterSpacing: 0.4,
  },
});
