import React from 'react';
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import Colors from '../../utils/Colors';

const SplashScreen = ({navigation}) => {
  setTimeout(() => {
    navigation.replace('Onboarding');
}, 3000);

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.Base} barStyle='dark-content' />
      <Image
        source={require('../../assets/images/logo.png')}
        style={styles.logo}
      />
      <Text style={styles.text}>Create taste, not waste</Text>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.Base,
  },
  logo: {
    width: 174,
    height: 167,
    alignSelf: 'center',
    marginTop: 206,
  },
  text: {
    alignSelf: 'center',
    color: Colors.Primary,
    fontFamily: 'Roboto-Bold',
    fontSize: 18,
    lineHeight: 42.19,
    letterSpacing: 0.32
  },
});
