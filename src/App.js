import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import FlashScreen from './screen/Splash';
import OnboardingScreen from './screen/Onboarding';
import LoginScreen from './screen/Login';
import MainTab from './bottom_tabs';
import EditAkun from './screen/Edit_Akun';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Flash">
        <Stack.Screen
          name="Flash"
          component={FlashScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Onboarding"
          component={OnboardingScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={MainTab}
          options={{headerShown: false}}
        />
        <Stack.Screen name="Edit Profil Toko" component={EditAkun} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
