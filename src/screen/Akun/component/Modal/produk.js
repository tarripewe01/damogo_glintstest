import React from 'react';
import {ImageBackground, StyleSheet, Text, View, Image} from 'react-native';

const Produk = () => {
  return (
    <View>
      <View style={{flexDirection: 'row'}}>
        <ImageBackground
          source={require('../../../../assets/images/starOn.png')}
          style={{height: 30, width: 30, marginVertical: 25}}>
          <Text
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              fontSize: 15,
              fontWeight: '500',
              color: '#fff',
              paddingVertical: 5,
            }}>
            1
          </Text>
        </ImageBackground>
        <View style={{paddingLeft: 10}}>
          <Image
            source={require('../../../../assets/images/produk.png')}
            style={{height: 79, width: 79}}
          />
        </View>
        <View style={{marginHorizontal: 10}}>
          <Text style={{fontSize: 15, fontWeight: '700'}}>
            Salad Moi Reguler Size
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginTop: 3,
            }}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                color: '#A0A0A0',
                textDecorationLine: 'line-through',
              }}>
              Rp12.000{' '}
            </Text>
            <Text style={{fontSize: 14, fontWeight: '700', color: '#009673'}}>
              Rp9.000{' '}
            </Text>
            <Text style={{fontSize: 12, fontWeight: '500', color: '#A0A0A0'}}>
              (30%)
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 10,
              marginLeft: -5,
            }}>
            <View
              style={{
                width: 75,
                height: 24,
                backgroundColor: '#F4F4F4',
                borderRadius: 3,
              }}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: '500',
                  color: '#A0A0A0',
                  alignSelf: 'center',
                  paddingVertical: 3,
                }}>
                24 Terjual
              </Text>
            </View>
            <View
              style={{
                width: 75,
                height: 24,
                backgroundColor: '#00A3E0',
                borderRadius: 3,
              }}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: '500',
                  color: '#fff',
                  alignSelf: 'center',
                  paddingVertical: 3,
                }}>
                Jual lagi
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          borderColor: '#E7E7E7',
          borderBottomWidth: 1,
          marginTop: 15,
        }}></View>

      <View style={{flexDirection: 'row', marginTop: 10}}>
        <ImageBackground
          source={require('../../../../assets/images/startOff.png')}
          style={{height: 30, width: 30, marginVertical: 25}}>
          <Text
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              fontSize: 15,
              fontWeight: '500',
              color: '#fff',
              paddingVertical: 5,
            }}>
            2
          </Text>
        </ImageBackground>
        <View style={{paddingLeft: 10}}>
          <Image
            source={require('../../../../assets/images/produk.png')}
            style={{height: 79, width: 79}}
          />
        </View>
        <View style={{marginHorizontal: 10}}>
          <Text style={{fontSize: 15, fontWeight: '700'}}>
            Salad Moi Reguler Size
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginTop: 3,
            }}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                color: '#A0A0A0',
                textDecorationLine: 'line-through',
              }}>
              Rp12.000{' '}
            </Text>
            <Text style={{fontSize: 14, fontWeight: '700', color: '#009673'}}>
              Rp9.000{' '}
            </Text>
            <Text style={{fontSize: 12, fontWeight: '500', color: '#A0A0A0'}}>
              (30%)
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 10,
              marginLeft: -5,
            }}>
            <View
              style={{
                width: 75,
                height: 24,
                backgroundColor: '#F4F4F4',
                borderRadius: 3,
              }}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: '500',
                  color: '#A0A0A0',
                  alignSelf: 'center',
                  paddingVertical: 3,
                }}>
                18 Terjual
              </Text>
            </View>
            <View
              style={{
                width: 75,
                height: 24,
                backgroundColor: '#00A3E0',
                borderRadius: 3,
              }}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: '500',
                  color: '#fff',
                  alignSelf: 'center',
                  paddingVertical: 3,
                }}>
                Jual lagi
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          borderColor: '#E7E7E7',
          borderBottomWidth: 1,
          marginTop: 15,
        }}></View>

<View style={{flexDirection: 'row', marginTop: 10}}>
        <ImageBackground
          source={require('../../../../assets/images/startOff.png')}
          style={{height: 30, width: 30, marginVertical: 25}}>
          <Text
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              fontSize: 15,
              fontWeight: '500',
              color: '#fff',
              paddingVertical: 5,
            }}>
            3
          </Text>
        </ImageBackground>
        <View style={{paddingLeft: 10}}>
          <Image
            source={require('../../../../assets/images/produk.png')}
            style={{height: 79, width: 79}}
          />
        </View>
        <View style={{marginHorizontal: 10}}>
          <Text style={{fontSize: 15, fontWeight: '700'}}>
            Salad Moi Reguler Size
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginTop: 3,
            }}>
            <Text
              style={{
                fontSize: 12,
                fontWeight: '500',
                color: '#A0A0A0',
                textDecorationLine: 'line-through',
              }}>
              Rp12.000{' '}
            </Text>
            <Text style={{fontSize: 14, fontWeight: '700', color: '#009673'}}>
              Rp9.000{' '}
            </Text>
            <Text style={{fontSize: 12, fontWeight: '500', color: '#A0A0A0'}}>
              (30%)
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginTop: 10,
              marginLeft: -5,
            }}>
            <View
              style={{
                width: 75,
                height: 24,
                backgroundColor: '#F4F4F4',
                borderRadius: 3,
              }}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: '500',
                  color: '#A0A0A0',
                  alignSelf: 'center',
                  paddingVertical: 3,
                }}>
                12 Terjual
              </Text>
            </View>
            <View
              style={{
                width: 75,
                height: 24,
                backgroundColor: '#00A3E0',
                borderRadius: 3,
              }}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: '500',
                  color: '#fff',
                  alignSelf: 'center',
                  paddingVertical: 3,
                }}>
                Jual lagi
              </Text>
            </View>
          </View>
        </View>
      </View>
     
    </View>
  );
};

export default Produk;

const styles = StyleSheet.create({});
