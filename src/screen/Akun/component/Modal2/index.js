import React from 'react';
import {ImageBackground, StyleSheet, Text, View, Image} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Jam from './jam';

const Modal2 = () => {
  return (
    <View style={{width: 300, height: 210, borderRadius: 20}}>
      <View
        style={{
          justifyContent: 'space-evenly',
          marginBottom: 20,
          flexDirection: 'row',
          marginLeft: 65,
        }}>
        <Text style={{fontSize: 18, fontWeight: '700'}}>Jam Recommended</Text>
        <FontAwesome5
          name="times"
          size={14}
          style={{color: '#AAA', marginLeft: 45}}
        />
      </View>
      <Jam/>
    </View>
  );
};

export default Modal2;

const styles = StyleSheet.create({});
